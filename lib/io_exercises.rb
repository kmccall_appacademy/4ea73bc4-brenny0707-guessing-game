# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  guess_count = 0
  number = rand(100) + 1
  correct_guess = false

  until correct_guess
    puts "Please guess a number from 1-100!"
    guess = gets.chomp.to_i

    if guess == number
      guess_count += 1
      correct_guess = true
      puts "You got the right number! The number was #{number} and you guessed it in #{guess_count} times!"
    elsif guess > number
      guess_count += 1
      puts "Try again! Your guess of #{guess} was too high."
    elsif guess < number
      guess_count += 1
      puts "Try again! Your guess of #{guess} was too low."
    end
  end
end


#shuffled.txt, don't see specs to test with?
# puts "Please specify the file you wish to open"
# input_name = gets.chomp
# lines = File.readlines(input_name).shuffle
#
# File.open("#{input_name}-shuffled.txt", "w") do |f|
#   lines.each do |line|
#     f.puts line
#   end
# end
